# Imports

__author__ = 'kevin richard'
__filename__ = 'launch'

import argparse
import os
from os.path import expanduser


def main(_parser):
	target_directory = expanduser("~") + os.sep + ".IntelliJIdea13" + os.sep + "config"
	jetbrains_plugin_file = target_directory + os.sep + "disabled_plugins.txt"
	args = _parser.parse_args()
	if args.save:
		theme_file = "saved_themes" + os.sep + args.save
		copy_file_into(jetbrains_plugin_file, theme_file)
	if args.themes:
		print 'select theme from:'
		for theme in os.listdir("saved_themes"):
			print '	' + theme
	if args.load:
		# this should copy the file and write it over the disabled_plugins file
		theme = "saved_themes" + os.sep + args.load
		copy_file_into(theme, jetbrains_plugin_file)


def copy_file_into(source, dest):
	# open the two files
	print 'copy:'
	print '\t' + source
	print 'into:'
	print '\t' + dest
	source_file = open(source, 'r')
	dest_file = open(dest, 'w')
	# add each line from source to destination file
	for line in source_file:
		dest_file.write(line)
	# close the files
	source_file.close()
	dest_file.close()


if __name__ == '__main__':
	print expanduser("~")
	parser = argparse.ArgumentParser()
	parser.add_argument("-l", "--load", help="Load theme from saved list")
	parser.add_argument("-t", "--themes", help="List out the saved themes", action="store_true")
	parser.add_argument("-s", "--save", help="Save the current disabled_plugins.txt as name")
	main(parser)
